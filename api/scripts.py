
def calculate_rank(lista_escritorios):
    # Com base no desempenho do mês, essa função calcula o ranking de especialidades da empresa.
    # Cálculo: taxa_de_sucesso/produto e redefinição do banco de dados
    # Não há retorno
    return


def distribution(office_name):
    # As empresas receberão em maior quantidade os casos de sua especialidade e também receberão os demais casos
    # em quantidade menor. O ajuste dessa quantidade será feito ao longo do processo para ajuste do grau
    # de significância.
    ranking = ['consignado', 'credito', 'imoveis', 'varejo']  # ordem de desempenho do office_name
    groups = {'consignado': 70, 'credito': 5, 'imoveis': 5, 'varejo': 5}
    return groups

