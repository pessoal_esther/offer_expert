from django.http import JsonResponse
from django.shortcuts import render
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view


@swagger_auto_schema(methods=['get'], tags=["Processo diário"], operation_id="Recomendações de ofertas customizadas")
@api_view(['GET'])
def individual_recomendation_view(request, *args, **kwargs):
    return JsonResponse({'Escritorio1': {'produto1': 70, 'produto2': 5},
                         'Escritorio2': {'produto2': 60, 'produto1': 5},
                         })


@swagger_auto_schema(methods=['get'], tags=["Processo diário"],
                     operation_id="Envio de listas de priorização para os escritórios")
@api_view(['GET'])
def priorization_to_office_view(request, *args, **kwargs):
    return JsonResponse({'Escritorio1': {'produto1': 70, 'produto2': 5},
                         'Escritorio2': {'produto2': 60, 'produto1': 5},
                         })


@swagger_auto_schema(methods=['get'], tags=["Sob demanda"],
                     operation_id="Varredura da base de clientes recebidas e redirecionamento"
                                  " para os escritórios mais adequados")
@api_view(['GET'])
def redirecting_view(request, *args, **kwargs):
    return JsonResponse({'response': [
                             {"id": "CPF1", "escritorio": "Escritorio1"},
                             {"id": "CPF2", "escritorio": "Escritorio1"},
                             {"id": "CPF3", "escritorio": "Escritorio2"},
                             {"id": "CPF4", "escritorio": "Escritorio3"}
                         ]})


@swagger_auto_schema(methods=['get'], tags=["Análise"],
                     operation_id="Captura dos dados para análise ")
@api_view(['GET'])
def collect_data_view(request, *args, **kwargs):
    return JsonResponse({'response': 'Lista de dados para a construção de visualizações'})
