# Generated by Django 4.0.3 on 2023-09-17 08:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Credito',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nome', models.CharField(blank=True, max_length=100, null=True)),
                ('valor', models.CharField(blank=True, max_length=200, null=True)),
                ('idade', models.IntegerField(blank=True, null=True)),
                ('categoria', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Devedor',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nome_completo', models.CharField(blank=True, max_length=100, null=True)),
                ('cpf', models.CharField(blank=True, max_length=200, null=True)),
                ('sexo', models.CharField(blank=True, max_length=2, null=True)),
                ('idade', models.IntegerField(null=True)),
                ('estado_residencia', models.CharField(blank=True, max_length=2, null=True)),
                ('regime_trabalho', models.CharField(blank=True, max_length=5, null=True)),
                ('remuneracao', models.CharField(blank=True, max_length=50, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='oferta',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('cedente', models.CharField(blank=True, max_length=200, null=True)),
                ('credito', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='api.credito')),
                ('devedor', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='api.devedor')),
            ],
        ),
        migrations.CreateModel(
            name='Especialidade',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('ordem', models.IntegerField(null=True)),
                ('credito', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='api.credito')),
            ],
        ),
        migrations.CreateModel(
            name='Escritorio',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nome', models.CharField(blank=True, max_length=100, null=True)),
                ('cnpj', models.CharField(blank=True, max_length=200, null=True)),
                ('especialidades', models.ManyToManyField(related_name='escrit_especial', to='api.especialidade')),
            ],
        ),
    ]
