from django.db import models


class Devedor(models.Model):
    # pessoa com dívida
    id = models.AutoField(primary_key=True)
    nome_completo = models.CharField(null=True, blank=True, max_length=100)
    cpf = models.CharField(null=True, blank=True, max_length=200)
    sexo = models.CharField(null=True, blank=True, max_length=2)
    idade = models.IntegerField(null=True)
    estado_residencia = models.CharField(null=True, blank=True, max_length=2)
    regime_trabalho = models.CharField(null=True, blank=True, max_length=5)
    remuneracao = models.CharField(null=True, blank=True, max_length=50)


class Credito(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(null=True, blank=True, max_length=100)
    valor = models.CharField(null=True, blank=True, max_length=200)
    idade = models.IntegerField(null=True, blank=True)
    categoria = models.IntegerField(null=True, blank=True)


class Especialidade(models.Model):
    id = models.AutoField(primary_key=True)
    credito = models.ForeignKey(Credito, on_delete=models.CASCADE, null=True)
    ordem = models.IntegerField(null=True)


class Escritorio(models.Model):
    # Escritório intermediando a oferta
    id = models.AutoField(primary_key=True)
    nome = models.CharField(null=True, blank=True, max_length=100)
    cnpj = models.CharField(null=True, blank=True, max_length=200)
    especialidades = models.ManyToManyField(Especialidade, related_name="escrit_especial")


class oferta(models.Model):
    # A oferta não depende do escritório.
    id = models.AutoField(primary_key=True)
    cedente = models.CharField(max_length=200, null=True, blank=True)
    devedor = models.ForeignKey(Devedor, on_delete=models.CASCADE, null=True)
    credito = models.ForeignKey(Credito, on_delete=models.CASCADE, null=True)
