# R2 Performance (antigo OfferExpert)

A recuperação de crédito é um desafio complexo, cuja solução pode 
envolver a combinação de estratégias e abordagens diferentes.
Entre as principais considerações estão a análise de dados,
negociação eficaz, automação de processos e a adoção de tecnologias avançadas.

No entanto, é importante considerar o aspecto dos custos da operação de recuperação
de crédito, desde a alocação de recursos para a contratação de profissionais 
qualificados, sistemas de tecnologia avançada, análise de dados e até 
mesmo despesas legais representanado um ônus financeiro significativo para as empresas.

A presente API Rest visa fornecer ferramentas que potencializem a atuação dos profissionais
de atendimento, primeiro, direcionando os tipos de cobranças a escritóros com comprovado maior
desempenho em determinados segmentos e em seguida determinando qual a melhor oferta para cada tipo de devedor.

Documentação disponível via Swagger em: http://192.168.125.211:8000/

## Informações sobre o processo de desenvolvimento do MVP
Para a construção de qualquer modelo preditivo supervisionado, tal qual optamos aqui, são necessários classes classificatórias.
No nosso caso, temos duas classes: sucesso versus insucesso nas negociações de cobrança. 
Recebemos uma planilha básica com essas classes e mais alguns atributos tais como
data do acordo, totoal de parcelas, região entre outros. No entando, um atributo fundamental para nossa modelagem não estava
presente e só poderia ser obtido junto a empresa contratante que era **qual escritório atuou no contato**.
Com isso, fomos forçados a gerar dados artificiais com base em estatísticas divulgadas em pesquisas.
A tabela final gerada encontra-se aqui: 
[dados_desafio3.csv](https://gitlab.com/pessoal_esther/offer_expert/-/blob/main/mockdata/dados_desafio3.csv)


### Problema inicial a ser atacado: distribuição aleatória
A ausência de análise de dados de performance das empresas parceiras anterior a distribuição de maneira aleatória, 
acarreta baixa performance em portfólios que eventualmente não tenham prática em determinados produtos.

![Ponto focal a ser atacado no primeiro mês](./static/images/foco2.png)

**Entregáveis**
Nossa aplicação não visa ser um B.I. porém visa fornecer os insumos necessários para uma análise de B.I. mais aprofundada.
Os dados de saída serão no formato JSON.

### Estrutura para futuros desenvolvimentos: modelagem de oferta
Para viabilizar a construção de um preditor de oferta é necessário a organização dos dados. Para isso, foi inicializada a modelagem 
banco de dados relacional como demonstrado na figura abaixo:

![diagrama de relacionamentos](./static/images/diagrama.png)


