import random as rd

f = open('mockdata_random.csv', 'w')
f.write('id,escritorio,produto,sucesso\n')
for i in range(1000):
    escritorio = rd.choices(['A', 'B', 'C', 'D'])[0]
    produto = f"P{rd.choices(list(range(1, 31)))[0]}"
    sucesso_random = rd.choices([False, False, False, False, True])[0]
    sucesso_alto = rd.choices([False, False, False, True, True])[0]
    if (escritorio == 'A' and produto == "P1") or (escritorio == 'B' and produto == "P2") or (escritorio == 'C' and produto == "P3") or (escritorio == 'D' and produto == "P4"):
        f.write(f"{i},{escritorio},{produto},{sucesso_alto}\n")
    else:
        f.write(f"{i},{escritorio},{produto},{sucesso_random}\n")
f.close()

f = open('mockdata_direcionado.csv', 'w')
f.write('id,escritorio,produto,sucesso\n')
for i in range(1000):
    escritorio = rd.choices(['A', 'B', 'C', 'D'])[0]
    produto = f"P{rd.choices(list(range(1, 31)))[0]}"
    sucesso_random = rd.choices([False, False, False, False, True])[0]
    sucesso_alto = rd.choices([False, False, False, True, True])[0]
    if (escritorio == 'A' and produto == "P1") or (escritorio == 'B' and produto == "P2") or (escritorio == 'C' and produto == "P3") or (escritorio == 'D' and produto == "P4"):
        f.write(f"{i},{escritorio},{produto},{sucesso_alto}\n")
    else:
        f.write(f"{i},{escritorio},{produto},{sucesso_random}\n")
f.close()




