import os
from drf_yasg import openapi
from django.urls import path, include
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from api.views import *

url = os.environ.get('URL')

schema_view = get_schema_view(
   openapi.Info(
      title="R2 Performance",
      default_version='v2023',
      description="Endpoints para recomendação e rankeamento para Return",
      contact=openapi.Contact(email="tec@gmail.com.br"),
   ),
   url=url,
   public=True,
   permission_classes=[permissions.AllowAny]
)

urlpatterns = [
    path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework_transaction')),
    path('recomedacao_por_cpf/<CPF>/', individual_recomendation_view, name='individual_recomendation_view'),
    path('lista_priorizacao/<escritorio>/', priorization_to_office_view, name='priorization_by_office_view'),
    path('redirecionamento/', redirecting_view, name='redirecting_view'),
    path('relatorio/', collect_data_view, name='collect_data_view')
]

